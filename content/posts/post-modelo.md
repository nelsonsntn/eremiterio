+++
title = "Post Modelo"
author = ["Nelson Silvério de Sant'Ana Neto"]
tags = ["tag1"]
categories = ["categoria1"]
draft = false
+++

Este é uma publicação teste, para verificar a diagramação do blog em vários formatos.


## Título 1 {#título-1}

Lorem[^fn:1] ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor[^fn:2] felis eros, vel mollis arcu varius in. Maecenas eget ultrices arcu, id varius orci. Sed facilisis aliquet luctus. Nam viverra odio vitae leo tempus, sit amet pulvinar turpis laoreet. Nulla viverra odio vestibulum, imperdiet felis quis, rutrum mauris. Phasellus non odio at lectus dignissim ultrices ut vitae lectus. Vivamus pulvinar ipsum id nisi fringilla ultrices. Aliquam efficitur velit sit amet urna dictum eleifend. Phasellus aliquam nec diam eu auctor. Nulla auctor nisl eu dapibus porta. Fusce rutrum, magna porttitor ornare congue, diam neque molestie augue, quis vulputate ligula ligula eget magna. Phasellus in turpis eu nisi luctus lacinia a vel nisi. Fusce in maximus orci. Pellentesque vitae viverra tortor.

-   Lista não numerada
    Lorem[^fn:1] ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor felis eros, vel mollis arcu varius in. Maecenas eget ultrices arcu, id varius orci. Sed facilisis aliquet luctus. Nam viverra odio vitae leo tempus, sit amet pulvinar turpis laoreet. Nulla viverra odio vestibulum, imperdiet felis quis, rutrum mauris. Phasellus non odio at lectus dignissim ultrices ut vitae lectus. Vivamus pulvinar ipsum id nisi fringilla ultrices. Aliquam efficitur velit sit amet urna dictum eleifend. Phasellus aliquam nec diam eu auctor. Nulla auctor nisl eu dapibus porta. Fusce rutrum, magna porttitor ornare congue, diam neque molestie augue, quis vulputate ligula ligula eget magna. Phasellus in turpis eu nisi luctus lacinia a vel nisi. Fusce in maximus orci. Pellentesque vitae viverra tortor.
-   Tópico 2
    -   Subtópico 1
    -   Subtótico 2
        Lorem[^fn:1] ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor felis eros, vel mollis arcu varius in. Maecenas eget ultrices arcu, id varius orci. Sed facilisis aliquet luctus. Nam viverra odio vitae leo tempus, sit amet pulvinar turpis laoreet. Nulla viverra odio vestibulum, imperdiet felis quis, rutrum mauris. Phasellus non odio at lectus dignissim ultrices ut vitae lectus. Vivamus pulvinar ipsum id nisi fringilla ultrices. Aliquam efficitur velit sit amet urna dictum eleifend. Phasellus aliquam nec diam eu auctor. Nulla auctor nisl eu dapibus porta. Fusce rutrum, magna porttitor ornare congue, diam neque molestie augue, quis vulputate ligula ligula eget magna. Phasellus in turpis eu nisi luctus lacinia a vel nisi. Fusce in maximus orci. Pellentesque vitae viverra tortor.

<!--listend-->

1.  Lista numerada
    Lorem[^fn:1] ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor felis eros, vel mollis arcu varius in. Maecenas eget ultrices arcu, id varius orci. Sed facilisis aliquet luctus. Nam viverra odio vitae leo tempus, sit amet pulvinar turpis laoreet. Nulla viverra odio vestibulum, imperdiet felis quis, rutrum mauris. Phasellus non odio at lectus dignissim ultrices ut vitae lectus. Vivamus pulvinar ipsum id nisi fringilla ultrices. Aliquam efficitur velit sit amet urna dictum eleifend. Phasellus aliquam nec diam eu auctor. Nulla auctor nisl eu dapibus porta. Fusce rutrum, magna porttitor ornare congue, diam neque molestie augue, quis vulputate ligula ligula eget magna. Phasellus in turpis eu nisi luctus lacinia a vel nisi. Fusce in maximus orci. Pellentesque vitae viverra tortor.
2.  Tópico 2
    1.  Subtópico 3
        Lorem[^fn:1] ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor felis eros, vel mollis arcu varius in. Maecenas eget ultrices arcu, id varius orci. Sed facilisis aliquet luctus. Nam viverra odio vitae leo tempus, sit amet pulvinar turpis laoreet. Nulla viverra odio vestibulum, imperdiet felis quis, rutrum mauris. Phasellus non odio at lectus dignissim ultrices ut vitae lectus. Vivamus pulvinar ipsum id nisi fringilla ultrices. Aliquam efficitur velit sit amet urna dictum eleifend. Phasellus aliquam nec diam eu auctor. Nulla auctor nisl eu dapibus porta. Fusce rutrum, magna porttitor ornare congue, diam neque molestie augue, quis vulputate ligula ligula eget magna. Phasellus in turpis eu nisi luctus lacinia a vel nisi. Fusce in maximus orci. Pellentesque vitae viverra tortor.

| Tabela 1 | Conteúdo | Etc   |
|----------|----------|-------|
| item     | cont 1   | etc 2 |
| item 2   | osdfal   | etc 2 |


### Sub Título {#sub-título}

Lorem[^fn:1] ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor felis eros, vel mollis arcu varius in. Maecenas eget ultrices arcu, id varius orci. Sed facilisis aliquet luctus. Nam viverra odio vitae leo tempus, sit amet pulvinar turpis laoreet. Nulla viverra odio vestibulum, imperdiet felis quis, rutrum mauris. Phasellus non odio at lectus dignissim ultrices ut vitae lectus. Vivamus pulvinar ipsum id nisi fringilla ultrices. Aliquam efficitur velit sit amet urna dictum eleifend. Phasellus aliquam nec diam eu auctor. Nulla auctor nisl eu dapibus porta. Fusce rutrum, magna porttitor ornare congue, diam neque molestie augue, quis vulputate ligula ligula eget magna. Phasellus in turpis eu nisi luctus lacinia a vel nisi. Fusce in maximus orci. Pellentesque vitae viverra tortor.


#### Sub Sub Título {#sub-sub-título}

Lorem[^fn:1] ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor felis eros, vel mollis arcu varius in. Maecenas eget ultrices arcu, id varius orci. Sed facilisis aliquet luctus. Nam viverra odio vitae leo tempus, sit amet pulvinar turpis laoreet. Nulla viverra odio vestibulum, imperdiet felis quis, rutrum mauris. Phasellus non odio at lectus dignissim ultrices ut vitae lectus. Vivamus pulvinar ipsum id nisi fringilla ultrices. Aliquam efficitur velit sit amet urna dictum eleifend. Phasellus aliquam nec diam eu auctor. Nulla auctor nisl eu dapibus porta. Fusce rutrum, magna porttitor ornare congue, diam neque molestie augue, quis vulputate ligula ligula eget magna. Phasellus in turpis eu nisi luctus lacinia a vel nisi. Fusce in maximus orci. Pellentesque vitae viverra tortor.

_Itálico_
**Negrito**
<span class="underline">traço baixo</span>
~~traçado~~
_**negrito e itálico**_
`código`
`verbatim`


## Listas {#listas}

Lista Exemplo:

1.  Primeiro Ítem
2.  Segundo Ítem
    1.  Primeiro Sub ítem
        -   Lista Desordenada
        -   Outro item Desordenado
            -   Assim também pode
                -   Ou

Pode conter caixas de seleção

-   [ ] não iniciado
-   [-] em progresso
-   [X] concluído

Listas podem conter etiquetas e caixas de seleção ao mesmo tempo

[ ] frutas
: pegar maçãs

[X] vegetais
: pegar cenouras


## Links {#links}

[Org Mode](https://orgmode.org)

[Listas](#listas)


## Imagens {#imagens}

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/5/5d/Konigsberg_bridges.png" >}}

{{< figure src="~/Documentos/pessoal/eremiterio/static/a-cool-guide-on-emailing-like-a-boss-v0-f98jj46mrs7d1.webp" >}}

{{< figure src="/Konigsberg_bridges.png" >}}


## Blocos {#blocos}

```text
exemplo de texto monoespaçado
```

> Everything should be made as simple as possible, but not any simpler ---Albert Einstein

<style>.org-center { margin-left: auto; margin-right: auto; text-align: center; }</style>

<div class="org-center">

Everything shoud be made as simple as possible,
but not any simpler

</div>

```emacs-lisp
(message "Hello World")
```

> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor felis eros, vel mollis arcu varius in. Maecenas eget ultrices arcu, id varius orci. Sed facilisis aliquet luctus. Nam viverra odio vitae leo tempus, sit amet pulvinar turpis laoreet. Nulla viverra odio vestibulum, imperdiet felis quis, rutrum mauris. Phasellus non odio at lectus dignissim ultrices ut vitae lectus. Vivamus pulvinar ipsum id nisi fringilla ultrices. Aliquam efficitur velit sit amet urna dictum eleifend. Phasellus aliquam nec diam eu auctor. Nulla auctor nisl eu dapibus porta. Fusce rutrum, magna porttitor ornare congue, diam neque molestie augue, quis vulputate ligula ligula eget magna. Phasellus in turpis eu nisi luctus lacinia a vel nisi. Fusce in maximus orci. Pellentesque vitae viverra tortor.&nbsp;[^fn:3]

[^fn:1]: Nota de Rodapé
[^fn:2]: nota de ropadé de teste
[^fn:3]: referência da citação